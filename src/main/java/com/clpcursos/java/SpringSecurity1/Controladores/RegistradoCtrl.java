package com.clpcursos.java.SpringSecurity1.Controladores;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/registrado")
public class RegistradoCtrl {

    @GetMapping({"","/"})
    public ModelAndView inicioRegistrado(){
        ModelAndView mav = new ModelAndView("Registrado");
        mav.addObject("titulo", "Aquí está el contenido de la página de inicio para un usuario registrado");
        return mav;
    }
    @GetMapping("/1")
    public ModelAndView Perfil1(){
        ModelAndView mav = new ModelAndView("Perfil1");
        mav.addObject("titulo", "Aquí hay contenido para un usuario registrado de perfil 1");
        return mav;
    }
    @GetMapping("/2")
    public ModelAndView Perfil2(){
        ModelAndView mav = new ModelAndView("Perfil2");
        mav.addObject("titulo", "Aquí hay contenido para un usuario registrado de perfil 2");
        return mav;
    }
    @GetMapping("/11")
    public ModelAndView Perfil11(){
        ModelAndView mav = new ModelAndView("Perfil11");
        mav.addObject("titulo", "Aquí hay contenido para un usuario registrado de perfil 1_1");
        return mav;
    }@GetMapping("/12")
    public ModelAndView Perfil12(){
        ModelAndView mav = new ModelAndView("Perfil12");
        mav.addObject("titulo", "Aquí hay contenido para un usuario registrado de perfil 1_2");
        return mav;
    }@GetMapping("/13")
    public ModelAndView Perfil13(){
        ModelAndView mav = new ModelAndView("Perfil13");
        mav.addObject("titulo", "Aquí hay contenido para un usuario registrado de perfil 1_3");
        return mav;
    }
    @GetMapping("/21")
    public ModelAndView Perfil21(){
        ModelAndView mav = new ModelAndView("Perfil21");
        mav.addObject("titulo", "Aquí hay contenido para un usuario registrado de perfil 2_1");
        return mav;
    }
    @GetMapping("/22")
    public ModelAndView Perfil22(){
        ModelAndView mav = new ModelAndView("Perfil22");
        mav.addObject("titulo", "Aquí hay contenido para un usuario registrado de perfil 2_2");
        return mav;
    }

}
