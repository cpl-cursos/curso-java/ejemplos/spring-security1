package com.clpcursos.java.SpringSecurity1.Controladores;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PublicoCtrl {

    @GetMapping({"","/"})
    public ModelAndView inicio(){
        ModelAndView mav = new ModelAndView("Inicio");
        mav.addObject("titulo", "Aquí está el contenido de la página de inicio");
        return mav;
    }

    @GetMapping("/publico")
    public ModelAndView Publico(){
        ModelAndView mav = new ModelAndView("publico1");
        mav.addObject("titulo", "Aquí hay contenido público");
        return mav;
    }
    @GetMapping("/publico/1")
    public ModelAndView Publico1(){
        ModelAndView mav = new ModelAndView("publico11");
        mav.addObject("titulo", "Aquí hay contenido público de tipo 1");
        return mav;
    }
    @GetMapping("/publico/2")
    public ModelAndView Publico2(){
        ModelAndView mav = new ModelAndView("publico12");
        mav.addObject("titulo", "Aquí hay contenido público de tipo 2");
        return mav;
    }
    @GetMapping("/publico/3")
    public ModelAndView Publico3(){
        ModelAndView mav = new ModelAndView("publico13");
        mav.addObject("titulo", "Aquí hay contenido público de tipo 3");
        return mav;
    }



}
