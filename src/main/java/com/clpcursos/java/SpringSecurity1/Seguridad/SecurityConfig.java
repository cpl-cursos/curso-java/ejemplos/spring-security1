package com.clpcursos.java.SpringSecurity1.Seguridad;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{
        http
                .authorizeHttpRequests(req -> req
                        // Verificamos las URI y permitimos el acceso a todo el mundo a la parte pública y a la pág. de inicio
                        .requestMatchers("","/").permitAll()
                        .requestMatchers("/publico/**").permitAll()
                        // El acceso a los contenidos registrados sólo se permite a los usuarios autentificados mediante el login
                        .requestMatchers("/registrado/**").authenticated()
                ).formLogin(
                        // Indicamos el comportamiento de la pantalla de login
                        form -> form
                                .defaultSuccessUrl("/registrado")   // url a presentar cuando el login es correcto
                                .permitAll()    // el formulario de login es accesible a todo el mundo.
                ).logout(
                        logout -> logout
                                // Indicamos la url que se activará cuando se solicite el logout
                                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                                .permitAll()
                );
        return http.build();
    }
}
